#include<stdio.h>
#include<math.h>

void inputPoints(int *x1, int *y1, int *x2, int *y2);
float dis(int x1, int y1, int x2, int y2);
void output(float d);

int main()
{
	int x1, y1, x2, y2; float d;
	inputPoints(&x1, &y1, &x2, &y2);
	d = dis(x1, y1, x2, y2);
	output(d);
	return 0;
}

void inputPoints(int *x1, int *y1, int *x2, int *y2)
{
	printf("Enter point x1: ");
	scanf("%d", x1);
	printf("Enter point y1: ");
	scanf("%d", y1);
	printf("Enter point x2: ");
	scanf("%d", x2);
	printf("Enter point y2: ");
	scanf("%d", y2);
}

float dis(int x1, int y1, int x2, int y2)
{
	return sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2));
}

void output(float d)
{
	printf("Distance between the 2 points is %.2f \n", d);
}