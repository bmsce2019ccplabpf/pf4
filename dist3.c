#include<stdio.h>
#include<math.h>

struct point {
	float x, y;
};

typedef struct point point;

point getPoint()
{
	point values;
	printf("Value: ");
	scanf("%f", &values.x);
	printf("Value: ");
	scanf("%f", &values.y);
	return values;
}

float getDist(point a, point b)
{
	return sqrt(pow((b.x - a.x), 2) + pow((b.y - a.y), 2));
}

void output(float dist)
{
	printf("Distance between 2 points is %.2f\n", dist);
}

void main()
{
	printf
	    ("Enter points x1, y1, x2 and y2 in this order, press enter after entering each x or y value, do this 4 times for all 4 points to get distance.\n");
	point a, b;
	a = getPoint();
	b = getPoint();
	float dist = getDist(a, b);
	output(dist);
}