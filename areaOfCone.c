#include <stdio.h>
#include <math.h>

int main()
{
    int r, h, A;
    printf("To calculate the area of a cone, first enter the radius and then the height \n");
    scanf("%d%d", &r, &h);
    A = 3.14*r*(r + sqrt((r*r)+(h*h)));
    printf("The area is %d", A);
    return 0;
}
