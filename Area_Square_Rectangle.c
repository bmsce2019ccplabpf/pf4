#include <stdio.h>
#include <math.h>
int main()
{
    int l, w, area;
    printf("Enter the length and width of a rectangle or a square\n");
    scanf("%d%d", &l, &w);
    area = l * w;
    printf("The area is %d units squared\n", area);
    return 0;
}