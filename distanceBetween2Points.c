#include<stdio.h>
#include<math.h>
int main()
{
	int x1,x2,y1,y2;
	float distance;
	printf("Enter the coordinates of the points you want to find the distance between, start with entering the (x,y) coordinates of the first point\n");
	scanf("%d%d",&x1,&y1);
	printf("Enter the (x,y) coordinates of the second point\n");
	scanf("%d%d",&x2,&y2);
	distance=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
	printf("Distance = %f \n",distance);
	return 0;
}
