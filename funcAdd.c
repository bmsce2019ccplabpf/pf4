#include <stdio.h>

void input(int *a, int *b)
{
	printf("Enter 2 numbers to add\n");
	scanf("%d%d", a, b);
}

int add(int a,int b)
{
	int answer;
	answer = a + b;
	printf("Sum is %d\n", answer);
	return answer;
}

void output(int sum)
{
	printf("Sum is %d \n");
}

int main()
{
	int num1, num2, sum;
	input(&num1, &num2);
	sum = add(num1, num2);
	output(sum);
	return 0;
}