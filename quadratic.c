#include<stdio.h>
#include<math.h>

int main()
{
	int a, b, c, disc, r1, r2;
	printf("Enter the coeffecinents of the quadratic equation a, b, c \n");
	scanf("%d%d%d", &a, &b, &c);
	disc =  pow(b, 2) - (4 * a * c);
	if (disc == 0)
	{
		printf("Equal roots \n");
		r1 = (-b) / (2 * a);
		printf("Both roots are %d \n", r1);
	}
	else if (disc > 0)
	{
		r1 = ((-b) + (sqrt(disc))) / (2 * a);
		r2 = ((-b) - (sqrt(disc))) / (2 * a);
		printf("Roots are %d and %d \n", r1, r2);
	}
	else 
	{
		printf("No real roots \n");
	}
}