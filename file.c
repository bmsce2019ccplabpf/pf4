#include<stdlib.h>
#include<stdio.h>
int main()
{
	int rollNo;
	char str[20];
	FILE *fp;
	printf("Enter name and roll number\n");
	scanf("%s%d", str, &rollNo);
	fp = fopen("INPUT", "w");
	if (fp == NULL) {
		printf("Error");
		exit(1);
	}
	fprintf(fp, "%s%d", str, rollNo);
	fclose(fp);
	fp = fopen("INPUT", "r");
	if (fp == NULL) {
		printf("Error");
		exit(1);
	}
	scanf(fp, "%s%d", str, &rollNo);
	printf("roll number is = %d, name is %s \n", rollNo, str);
	fclose(fp);
	return 0;
}