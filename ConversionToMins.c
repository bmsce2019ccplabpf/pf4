#include<stdio.h>

void convert(int *a, int *b)
{
	printf("Enter time in hours then in minutes\n");
	scanf("%d%d", a, b);
	int answer;
	answer = *a * 60 + *b;
	printf("Total minutes is %d\n", answer);
}

int main()
{
	int hours, minutes;
	convert(&hours, &minutes);
	return 0;
}