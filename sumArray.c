#include<stdio.h>

void getValues(int n, int nums[n]);
int sumOfNums(int n, int nums[n]);
void result(int sum);

int main()
{
	int n, sum;
	printf
	    ("This is a program to find the sum of n different numbers\nTo proceed enter how many different numbers you want to add\n");
	scanf("%d", &n);
	int nums[n];
	getValues(n, nums);
	sum = sumOfNums(n, nums);
	result(sum);
	return 0;
}

void getValues(int n, int nums[n])
{
	printf("Enter the values of the numbers you want to add\n");
	for (int i = 0; i < n; i++) {
		scanf("%d", &nums[i]);
	}
}

int sumOfNums(int n, int nums[n])
{
	int sum = 0;
	for (int i = 0; i < n; i++) {
		sum += nums[i];
	}
	return sum;
}

void result(int sum)
{
	printf("The sum of n different numbers is %d\n", sum);
}