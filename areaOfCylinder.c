#include <stdio.h>
#include <math.h>

int main()
{
    int r, h, A;
    printf("To calculate the area of a cylinder, first enter the radius and then the height \n");
    scanf("%d%d", &r, &h);
    A = (2*3.14*r*h) + (2*3.14*r*r);
    printf("The area is %d", A);
    return 0;
}
