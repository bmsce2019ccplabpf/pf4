#include<stdio.h>

void get3Numbers(int *a, int *b, int *c);
int findLargest(int a, int b, int c);
void answer(int ans);

int main()
{
	int a, b, c, largest;
	get3Numbers(&a, &b, &c);
	largest = findLargest(a, b, c);
	answer(largest);
}

void get3Numbers(int *a, int *b, int *c)
{
	printf("Enter 3 numbers to find largest of them \n");
	printf("Number 1: ");
	scanf("%d", a);
	printf("Number 2: ");
	scanf("%d", b);
	printf("Number 3: ");
	scanf("%d", c);
}

int findLargest(int a, int b, int c)
{
	return a>b?(a>c?a:c):(b>c?b:c);
}

void answer(int ans)
{
    printf("Largest number is : %d \n", ans);
}