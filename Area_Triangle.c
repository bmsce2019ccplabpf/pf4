#include <stdio.h>
#include <math.h>
int main()
{
    int s1, s2, s3, s, area;
    printf("Enter the length of the three sides of a triangle\n");
    scanf("%d%d%d", &s1, &s2, &s3);
    s = (s1 + s2 + s3)/2;
    area = sqrt(s*(s-s1)*(s-s2)*(s-s3));
    printf("The area is %d units squared\n", area);
    return 0;
}